"""Build streamlit app for Taxi Example Using Reinforcement Learning
"""

import streamlit as st
import streamlit.components.v1 as components

from analysis import Analysis
from model import Model

model = Model()
analysis = Analysis()

st.header("Self-driving Taxi Using Reinforcement Learning")
st.write(
    "This application uses [OpenAi Gym]"
    "(https://github.com/openai/gym/blob/master/gym/envs/toy_text/taxi.py)"
    " to train a Reinforcement Learning (RL) algorithm (Q-learning)"
)

st.subheader("Train and Evaluate RL Algorithm")
with st.form("train_eval_parameters"):
    col_train, col_eval = st.columns(2)

    with col_train:
        st.subheader("Training")
        n_episodes_train = st.slider(
            "Number of episodes:",
            0,
            8000,
            5000,
            1,
            key="n_episodes_train",
            help="Each episode represents one complete session \
            and is independent from other episodes.",
        )
        max_steps_train = st.slider(
            "Maximum number of steps per episode (train):",
            0,
            100,
            90,
            1,
            key="max_steps_train",
        )
        learning_rate_train = st.slider(
            "Learning rate:",
            0.0,
            1.0,
            0.7,
            0.01,
            help="How much to accept new information vs. old information \
                (0: no update from new information).",
        )
        discount_rate_train = st.slider(
            "Discount rate:",
            0.0,
            1.0,
            0.62,
            0.01,
            help="Discount the \
                future rewards.",
        )
        exploration_probability_train = st.slider(
            "Exploration probability:",
            0.0,
            1.0,
            0.2,
            0.01,
            key="exploration_probability_train",
            help="probability on when to select exploration over exploitation.",
        )

    with col_eval:
        st.subheader("Evaluate")
        n_episodes_test = st.slider(
            "Number of episodes:",
            0,
            8000,
            1000,
            1,
            help="Each episode represents one complete session \
            and is indepent from other episodes.",
        )
        max_steps_test = st.slider(
            "Maximum number of steps per episode:", 0, 100, 90, 1
        )
        exploration_probability_test = st.slider(
            "Exploration probability:",
            0.0,
            1.0,
            0.1,
            0.01,
            help="probability on when to select exploration over exploitation.",
        )

    # Every form must have a submit button.
    submitted = st.form_submit_button("Start")
    if submitted:
        with st.spinner("Training..."):
            rewards_train = model.train(
                n_episodes=n_episodes_train,
                max_steps=max_steps_train,
                learning_rate=learning_rate_train,
                discount_rate=discount_rate_train,
                exploration_probability=exploration_probability_train,
            )
        st.success("Training is done!")

        with st.spinner("Evaluating..."):
            rewards_test, frames = model.evaluate(
                n_episodes=n_episodes_test,
                max_steps=max_steps_test,
                exploration_probability=exploration_probability_test,
            )
        st.success("Evaluation is done!")

        with st.spinner("Generating animation..."):
            episode_k = 1
            anim = analysis.render(
                analysis.extract_render_info(frames, episode_k)
            )
        st.success("Animation generation is done!")
    else:
        anim = None
        st.warning("Please start.")

st.subheader("Self-Driving Taxi Animation")
st.write(
    "A taxi (a yellow box) will pickup a passenger at a blue circle. \
    Once picked up, the taxi changes to green box. \
    Then the taxi will move and drop the passenger at a purple circle. "
)
st.write(
    "Each time, the pickup and drop off locations will be randomly \
    selected from 4 locations. The start position of taxi will also be random."
)
if anim is not None:
    st.info("Click buttons below to run the animation")
    components.html(anim.to_jshtml(), height=750)
else:
    st.warning("No animation yet. Please start training first")
