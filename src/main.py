"""
main module

Load configuration file, train RL agent, evaluate results, and draw animation.
"""
import os

from analysis import Analysis
from init import Init
from model import Model


def main():
    """main function to train, evaluate, and analyze RL results."""
    # Initialization
    base_dir = os.getcwd()
    config_dir = os.path.join(base_dir, "config")
    init = Init(base_dir=config_dir, config_file_name="main.yml")
    config = init.get_config()

    # Parameters
    env = config['env']
    train_param = config["training"]
    evaluation_param = config["evaluation"]

    # Training and evaluate agent
    model = Model(env["space_x"], env["space_y"])
    rewards_train = model.train(
        n_episodes=train_param["episodes"],
        max_steps=train_param["max_steps"],
        learning_rate=train_param["learning_rate"],
        discount_rate=train_param["discount_rate"],
        exploration_probability=train_param["exploration_probability"],
        use_prev_training_model=train_param["use_prev_training_model"],
        training_model_file=train_param["training_model_file"],
        taxi_fuel=env["taxi_fuel"],
    )
    rewards_test, frames = model.evaluate(
        n_episodes=evaluation_param["episodes"],
        max_steps=evaluation_param["max_steps"],
        exploration_probability=evaluation_param["exploration_probability"],
        taxi_fuel=env["taxi_fuel"],
    )

    # Analysis
    analysis = Analysis()
    analysis.plot_rewards(
        rewards_train, title="Sum of Rewards over Episode (Training)"
    )
    analysis.plot_rewards(
        rewards_test, title="Sum of Rewards over Episode (Evaluation)"
    )

    episode_k = 1
    analysis.render(frames, episode_k)


if __name__ == "__main__":
    main()
