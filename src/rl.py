"""Reinforcement Learning module

Contains class and functions related with reinforcement learning
"""

import numpy as np


class Agent:
    """A class representing Agent used in reinforcement learning.

    Wrap up all agent related properties (e.g., q table for Q learning) and methods (e.g., learning, choose_action).

    Attributes
    __________
    n_actions: int
        Number of actions.
    q_table: 2d array of int
        A table to store leaning parameters in the Q learning method.
    learning_rate: float
        Learning rate to determine how much new information will be considered.
    discount_rate: float
        Discount rate to discount the future reward.

    Methods
    _______
    choose_action(state, epsilon=0.01)
        Choose action based on the current state and exploration probability (epsilon).
    learn(state, action, reward, next_state)
        Train the agent by using the information from the environment.
    """

    def __init__(self, n_states, n_actions, learning_rate=0.7, discount_rate=0.618):
        """Agent class constructor.

        Parameters
        ----------
        n_states : int
            Number of states of the environment.
        n_actions : int
            Number of action of the environment.
        learning_rate : float
            Learning rate to determine how much new information will be considered.
        discount_rate : float
            Discount rate to discount the future reward.
        """

        # Todo: Add action space as an input
        self.n_actions = n_actions
        self.q_table = np.zeros((n_states, n_actions), dtype=np.float32)  # use 32 bit to reduce array size
        self.learning_rate = learning_rate
        self.discount_rate = discount_rate

    def choose_action(self, state, epsilon=0.01):
        """Choose action.

        Choose action based the current state, exploration probability, and q_table values.

        Parameters
        ----------
        state : int
            Current state.
        epsilon : float
            Exploration probability to choose between exploration and exploitation.

        Returns
        -------
        int
            integer value to represent action.
        """
        exploration_tradeoff = np.random.uniform(0, 1)
        if exploration_tradeoff < epsilon:
            return np.random.randint(
                self.n_actions
            )  # assume action space is between 0 and n_actions
        else:
            if hasattr(state, '__len__'):
                state = state[0]
            return np.argmax(self.q_table[state, :])

    def learn(self, state, action, reward, next_state):
        """Learn method.

        Agent learning method to update internal parameters like q_table using information from environment.
        References
        [1]R. S. Sutton and A. G. Barto, Reinforcement learning: an introduction, Second edition. Cambridge,
        Massachusetts: The MIT Press, 2018.

        Parameters
        ----------
        state : int
            Current state.
        action : int
            Current action.
        reward :
            Current reward from the environment.
        next_state :
            Next state.
        """
        old_q_value = self.q_table[state, action]
        next_max = np.max(self.q_table[next_state, :])
        # Update q table using Eq. 6.8 in Sutton's RL book [1]
        self.q_table[state, action] = (
            1 - self.learning_rate
        ) * old_q_value + self.learning_rate * (reward + self.discount_rate * next_max)

    def update_q_table(self, q_table):
        """Update internal variable q_table

        Parameters
        ----------
        q_table : numpy 2d array
            _description_
        """
        if self.q_table.shape == q_table.shape:
            self.q_table = q_table
        else:
            raise ValueError(f"Input Q table ({q_table.shape[0]} x {q_table.shape[1]}) is different\
                from the required dimension ({self.q_table.shape[0]} x {self.q_table.shape[1]})")
