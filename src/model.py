"""
Model module

Train and evaluate reinforcement learning (RL) agent.
"""

import os
import sys
import time

import numpy as np

from rl import Agent

sys.path.append(os.path.join(os.getcwd(), "custom_envs/cab_env"))
from cab_main import CityCabEnv

from utility import progressbar


class Model:
    """A class for modeling

    This model class contains training and evaluation of reinforcement learning
    agent using OpenAI gym environment.

    Attributes
    __________
    env: gym.envs.toy_text.taxi.TaxiEnv
        An OpenAI gym environment for taxi (text-based environment).
    agent: Agent
        An agent for reinforcement learning.

    Methods
    _______
    train(n_episodes=1000, max_steps=99, learning_rate=0.7, discount_rate=0.618,
     exploration_probability=0.2)
        Train the agent using the taxi environment.
    evaluate(n_episodes=10, max_steps=99, exploration_probability=0.2)
        Evaluate the trained agent.
    """

    def __init__(self, env_space_x=600, env_space_y=800):
        """Model class constructor

        Create the taxi environment and agent.
        """
        # Add .evn to prevent environment stopping at default 200 iterations
        # self.env = gym.make("Taxi-v3").env
        self.env = CityCabEnv(space_x=env_space_x, space_y=env_space_y)

        # Export run to .json file
        # self.env = gym.wrappers.RecordVideo(self.env, './videos/'
        # + str(time.time()) + '/')

        # Dimension of state and action
        n_states = self.env.observation_space.n
        n_actions = self.env.action_space.n
        self.agent = Agent(n_states, n_actions)

    def __del__(self):
        """Model class destructor

        Close the taxi environment.
        """
        self.env.close()

    # @profile
    def train(
            self,
            n_episodes=100,
            max_steps=5000,
            learning_rate=0.7,
            discount_rate=0.618,
            exploration_probability=0.2,
            use_prev_training_model=False,
            training_model_file='q_table',
            taxi_fuel=5000,
    ):
        """Train the agent

        Train the agent via interacting with the environment and plot training
        results.

        Parameters
        ----------
        n_episodes : int
            Number of episodes (successful drop-off).
        max_steps : int
            Max number of steps per episode.
        learning_rate : float
            Learning rate to determine how much new information will be considered.
        discount_rate : float
            Discount rate to discount the future reward.
        exploration_probability : float
            Exploration probability to determine when to select exploration over
            exploitation.
        use_prev_training_model : bool
        training_model_file : str
        taxi_fuel : int
        """

        self.agent.learning_rate = learning_rate
        self.agent.discount_rate = discount_rate
        if use_prev_training_model:
            self.agent.update_q_table(np.load(training_model_file))

        print(f"Training (max steps: {max_steps}):")
        rewards = np.zeros(n_episodes)
        st_train = time.process_time()
        for episode in range(n_episodes):
            print(f'Training Episode {episode}/{n_episodes}..')
            st_ep = time.process_time()
            state = self.env.reset(fuel=taxi_fuel)
            rewards_per_episode = 0
            exectime_step = []

            # for _ in progressbar(range(max_steps), f"Episode {episode + 1}/{n_episodes}:", 40):
            for step in range(max_steps):
                st_step = time.process_time()

                # Choose action
                action = self.agent.choose_action(state, exploration_probability)

                # Take action and observe reward
                next_state, reward_per_step, done, info = self.env.step(action)

                # Agent learning
                self.agent.learn(state, action, reward_per_step, next_state)

                # Update to new state
                state = next_state
                rewards_per_episode += reward_per_step
                if step < 100:
                    exectime_step.append(time.process_time() - st_step)
                elif step == 100:
                    print(f'-->Average step execution time = {round(np.mean(exectime_step) * 1e3, 2)} msecs')

                # If done, finish episode
                if done:
                    break
            exectime_ep = time.process_time() - st_ep
            print(f'-->Episode execution time = {round(exectime_ep / 60, 2)} mins\n')

            # Append reward value for plotting
            rewards[episode] = rewards_per_episode

        exectime_train = time.process_time() - st_train
        print(f'Training execution time = {round(exectime_train / 60, 1)} mins\n')

        # Find number of rows in q_table that are all zeros, indicating these
        # states are not be updated by the training
        n_uncovered_states = np.count_nonzero(np.all(self.agent.q_table == 0, axis=1))
        n_covered_states = self.env.observation_space.n - n_uncovered_states
        print(
            f"Training covered {n_covered_states / self.env.observation_space.n * 100:.3f}% states"
        )
        print(f"Training completed over {n_episodes} episodes")
        np.save(training_model_file, self.agent.q_table)
        return rewards

    # @profile
    def evaluate(self, n_episodes=10, max_steps=500000, exploration_probability=0.2, taxi_fuel=50000):
        """Evaluate the trained agent

        Evaluate the trained agent on the environment and plot results.

        Parameters
        ----------
        n_episodes : int
            Number of episodes for evaluation.
        max_steps :  int
            Max number of steps per episode.
        exploration_probability : float
            Exploration probability to determine when to select exploration over
            exploitation.
        taxi_fuel : int
        """
        frames = []
        rewards = np.zeros(n_episodes)

        print("Evaluation:")
        for episode in range(n_episodes):
            state = self.env.reset(fuel=taxi_fuel)
            rewards_per_episode = 0

            frames.append(
                {
                    "frame": self.env.render_mode(mode="rgb_array"),
                    "state": state,
                    "episode": episode + 1,
                    "step": -1,
                    "reward": 0,
                }
            )  # Initial state

            for step in progressbar(range(max_steps), f"Episode {episode + 1}/{n_episodes}:", 40):
                # for step in range(max_steps):
                # print(f'TRAINED AGENT')
                # print(f'Step {step + 1}')

                action = self.agent.choose_action(state, exploration_probability)
                next_state, reward_per_step, done, info = self.env.step(action)
                # self.env.render(mode='human')

                frames.append(
                    {
                        "frame": self.env.render_mode(mode="rgb_array"),
                        "state": next_state,
                        "episode": episode + 1,
                        "step": step,
                        "reward": reward_per_step,
                    }
                )

                state = next_state
                rewards_per_episode += reward_per_step

                if done:
                    break

            rewards[episode] = rewards_per_episode

        return rewards, frames
