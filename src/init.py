"""
Initialization module

Load configuration form a yaml file
"""

import os
import yaml


class Init:
    """Initialization class

    Contains methods to load configuration file from a yaml file.

    Attributes
    __________
    base_dir: string
        base directory for the configuration file.
    config_name: configuration file name
        configuration file name.
    config: dictionary
        configuration parameters loaded from the yaml file.

    Methods
    _______
    check_dirs()
        Check whether the directory is valid.
    get_config()
        Get configuration parameters.
    """

    def __init__(self, base_dir, config_file_name):
        """Model class constructor

        Check whether the yaml config file exists and load configuration.

        Parameters
        ----------
        base_dir : strings
            The base directory that contains the yaml configuration file.
        config_file_name : strings
            The configuration file name.

        Raises
        ------
        FileNotFoundError
            The yaml configuration not found exception.
        exception
            The yaml file loading exception
        """
        self.base_dir = base_dir
        # self.data_dir = data_dir
        # self.models_dir = models_dir
        self.config_name = config_file_name
        if not self.check_dirs():
            print("Config file doesn't exist!")
            raise FileNotFoundError("Check file directory and name")
        with open(os.path.join(self.base_dir, self.config_name), "r") as stream:
            try:
                # Use safe_load() to prevent arbitrary code execution
                self.config = yaml.safe_load(stream)
            except yaml.YAMLError as exception:
                print(exception)
                raise exception
        # self.mode = self.config['mode']

    def check_dirs(self):
        """Check whether the configuration file exists

        Returns
        -------
        Boolean
            True: file exists; False: file doesn't exist.
        """
        # data_path = os.path.join(self.base_dir, self.data_dir)
        # if not os.path.isdir(data_path):
        #     return False
        config_path = os.path.join(self.base_dir, self.config_name)
        if not os.path.isfile(config_path):
            return False
        # models_path = os.path.join(self.base_dir, self.models_dir)
        # if not os.path.isdir(models_path)
        #     os.mkdir(models_path)
        return True

    def get_config(self):
        """Get configuration parameters

        Returns
        -------
        Dictionary
            Configuration parameters loading from the yaml file.
        """
        return self.config
