"""Analysis module

Contain functions for analyzing and plotting data. Currently, the Analysis class
is not used.
"""
import cv2
import matplotlib.pyplot as plt
import numpy as np

font = cv2.FONT_HERSHEY_COMPLEX_SMALL


class Analysis:
    """A class for rendering results

    This analysis class contains methods to print and render output of gym
    environment.

    Attributes
    __________


    Methods
    _______
        print_frames(frames)
            print frame information to terminal.

        plot_rewards(rewards, title)
            plot rewards.

        decode(i)
            decode state information to get taxi row, taxi column, passenger
            pickup location, and passenger drop off location.

        render(frame_render)
            render multiple frames in an animation.
    """

    def __init__(self):
        pass

    @staticmethod
    def plot_rewards(rewards, title="Reward per episode"):
        """Plot rewards from modeling

        Parameters
        ----------
        rewards : list of float
            Rewards of each episode from modeling.
        title : str, optional
            Title of the figure, by default 'Reward per episode'
        """
        plt.figure()
        plt.plot(rewards)
        plt.title(title)
        plt.xlabel("Episode")
        plt.ylabel("Rewards")
        plt.show(block=False)

    # @profile
    @staticmethod
    def render(frames, episode, video_output=True):
        frame_width, frame_height = None, None
        fps, fourcc = None, None
        video_out = None

        if video_output:
            fps = 30.0
            # fourcc = cv2.VideoWriter_fourcc(*'mjpg')
            fourcc = cv2.VideoWriter_fourcc(*'mp4v')

        for frame in frames:
            if frame['episode'] == episode:
                data = frame['frame']
                cv2.imshow("CityCab", data)
                cv2.waitKey(25)

                if video_output:
                    if frame_width is None or frame_height is None:
                        frame_width, frame_height = data.shape[0], data.shape[1]
                        # Need to revert width and height since cv2 expects dimension opposite of numpy
                        video_out = cv2.VideoWriter('city_cab.mp4', fourcc, fps, (frame_height, frame_width))

                    # OpenCV VideoWriter requires uint8 data type.
                    # Scaled by 255 to avoid losing precision.
                    video_out.write((data*255).astype(np.uint8))

        if video_output:
            video_out.release()

    # @profile
    @staticmethod
    def generate_animation_video(frames, episode):
        fps = 30.0
        # fourcc = cv2.VideoWriter_fourcc(*'mjpg')
        fourcc = cv2.VideoWriter_fourcc(*'mp4v')
        frame_width = None
        frame_height = None
        video_out = None

        for frame in frames:
            if frame['episode'] == episode:
                data = frame['frame']

                if frame_width is None or frame_height is None:
                    frame_width, frame_height = data.shape[0], data.shape[1]
                    # Need to revert width and height since cv2 expects dimension opposite of numpy
                    video_out = cv2.VideoWriter('city_cab.mp4', fourcc, fps, (frame_height, frame_width))

                # OpenCV VideoWriter requires uint8 data type.
                # Scaled by 255 to avoid losing precision.
                video_out.write((data*255).astype(np.uint8))

        video_out.release()
