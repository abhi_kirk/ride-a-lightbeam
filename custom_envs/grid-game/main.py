from grid_world import GridWorldEnv

env = GridWorldEnv(render_mode="human")
env.reset()

for _ in range(100):
    obs, reward, done, info = env.step(env.action_space.sample())
    if done:
        obs, info = env.reset(return_info=True)
