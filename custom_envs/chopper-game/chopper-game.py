import random

import cv2
import numpy as np
from gym import Env, spaces

font = cv2.FONT_HERSHEY_COMPLEX_SMALL


class ChopperScape(Env):
    def __init__(self):
        super().__init__()
        self.chopper = None
        self.fuel_count = None
        self.bird_count = None
        self.ep_return = None
        self.fuel_left = None
        self.observation_shape = (600, 800, 3)
        self.observation_space = spaces.Box(low=np.zeros(self.observation_shape),
                                            high=np.ones(self.observation_shape),
                                            dtype=float)
        self.action_space = spaces.Discrete(6, 0)
        self.canvas = np.ones(self.observation_shape) * 1
        self.elements = []
        self.max_fuel = 1000
        self.y_min = int(self.observation_shape[0] * 0.1)
        self.x_min = 0
        self.y_max = int(self.observation_shape[0] * 0.9)
        self.x_max = self.observation_shape[1]

    def draw_elements_on_canvas(self):
        self.canvas = np.ones(self.observation_shape) * 1
        for elem in self.elements:
            elem_shape = elem.icon.shape
            x, y = elem.x, elem.y
            self.canvas[y: y + elem_shape[1], x: x + elem_shape[0]] = elem.icon
        text = f'Fuel Left: {self.fuel_left} | Rewards: {self.ep_return}'
        self.canvas = cv2.putText(img=self.canvas, text=text, org=(10, 20), fontFace=font,
                                  fontScale=0.8, color=(0, 0, 0), thickness=1, lineType=cv2.LINE_AA)

    def reset(self, **kwargs):
        self.fuel_left = self.max_fuel
        self.ep_return = 0
        self.bird_count = 0
        self.fuel_count = 0
        x = random.randrange(int(self.observation_shape[0] * 0.05),
                             int(self.observation_shape[0] * 0.10))
        y = random.randrange(int(self.observation_shape[1] * 0.15),
                             int(self.observation_shape[1] * 0.20))
        self.chopper = Chopper("chopper", self.x_max, self.x_min, self.y_max, self.y_min)
        self.chopper.set_position(x, y)
        self.elements = [self.chopper]
        self.canvas = np.ones(self.observation_shape) * 1
        self.draw_elements_on_canvas()
        return self.canvas

    def render(self, mode="human"):
        assert mode in ["human", "rgb_array"], "Invalid mode, must be either \"human\" or \"rgb_array\""
        if mode == "human":
            cv2.imshow("Game", self.canvas)
            cv2.waitKey(10)
        elif mode == "rgb_array":
            return self.canvas

    def close(self):
        cv2.destroyAllWindows()

    @staticmethod
    def has_collided(elem1, elem2):
        x_col = False
        y_col = False
        elem1_x, elem1_y = elem1.get_position()
        elem2_x, elem2_y = elem2.get_position()
        if 2 * abs(elem1_x - elem2_x) <= (elem1.icon_w + elem2.icon_w):
            x_col = True
        if 2 * abs(elem1_y - elem2_y) <= (elem1.icon_h + elem2.icon_h):
            y_col = True
        if x_col and y_col:
            return True
        return False

    @staticmethod
    def get_action_meanings():
        return {0: "Right", 1: "Left", 2: "Down", 3: "Up", 4: "Do Nothing"}

    def step(self, action_):
        done_ = False
        assert self.action_space.contains(action_), "Invalid action"
        self.fuel_left -= 1
        reward_ = 1
        if action_ == 0:
            self.chopper.move(0, 5)
        elif action_ == 1:
            self.chopper.move(0, -5)
        elif action_ == 2:
            self.chopper.move(5, 0)
        elif action_ == 3:
            self.chopper.move(-5, 0)
        elif action_ == 4:
            self.chopper.move(0, 0)

        if random.random() < 0.01:
            spawned_bird = Bird("bird_{}".format(self.bird_count),
                                self.x_max, self.x_min, self.y_max, self.y_min)
            self.bird_count += 1
            bird_x = self.x_max
            bird_y = random.randrange(self.y_min, self.y_max)
            spawned_bird.set_position(bird_x, bird_y)
            self.elements.append(spawned_bird)

        if random.random() < 0.01:
            spawned_fuel = Fuel("fuel_{}".format(self.fuel_count),
                                self.x_max, self.x_min, self.y_max, self.y_min)
            self.fuel_count += 1
            fuel_x = self.x_max
            fuel_y = random.randrange(self.y_min, self.y_max)
            spawned_fuel.set_position(fuel_x, fuel_y)
            self.elements.append(spawned_fuel)

        for elem in self.elements:
            if isinstance(elem, Bird):
                # if the bird has reached the left edge, then remove
                if elem.get_position()[0] <= self.x_min:
                    self.elements.remove(elem)
                else:
                    elem.move(-5, 0)
                # if the bird has collided
                if self.has_collided(self.chopper, elem):
                    # conclude episode and remove chopper
                    done_ = True
                    reward_ = -10
                    self.elements.remove(self.chopper)
            if isinstance(elem, Fuel):
                # if the fuel has reached the left edge, then remove
                if elem.get_position()[0] <= self.x_min:
                    self.elements.remove(elem)
                else:
                    elem.move(-5, 0)
                # if the fuel has collided
                if self.has_collided(self.chopper, elem):
                    self.fuel_left = self.max_fuel
        self.ep_return += 1
        self.draw_elements_on_canvas()
        if self.fuel_left == 0:
            done_ = True
        return self.canvas, reward_, done_, []


class Point:
    def __init__(self, name, x_max, x_min, y_max, y_min):
        self.icon_h = None
        self.icon_w = None
        self.x = 0
        self.y = 0
        self.x_min = x_min
        self.x_max = x_max
        self.y_min = y_min
        self.y_max = y_max
        self.name = name

    @staticmethod
    def clamp(n, minn, maxn):
        return max(min(maxn, n), minn)

    def set_position(self, x, y):
        self.x = self.clamp(x, self.x_min, self.x_max - self.icon_w)
        self.y = self.clamp(y, self.y_min, self.y_max - self.icon_h)

    def get_position(self):
        return self.x, self.y

    def move(self, del_x, del_y):
        self.x += del_x
        self.y += del_y
        self.x = self.clamp(self.x, self.x_min, self.x_max - self.icon_w)
        self.y = self.clamp(self.y, self.y_min, self.y_max - self.icon_h)


class Chopper(Point):
    def __init__(self, name, x_max, x_min, y_max, y_min):
        super().__init__(name, x_max, x_min, y_max, y_min)
        self.icon = cv2.imread("chopper.png") / 255.0
        self.icon_w = 64
        self.icon_h = 64
        self.icon = cv2.resize(self.icon, (self.icon_h, self.icon_w))


class Bird(Point):
    def __init__(self, name, x_max, x_min, y_max, y_min):
        super().__init__(name, x_max, x_min, y_max, y_min)
        self.icon = cv2.imread("bird.png") / 255.0
        self.icon_w = 32
        self.icon_h = 32
        self.icon = cv2.resize(self.icon, (self.icon_h, self.icon_w))


class Fuel(Point):
    def __init__(self, name, x_max, x_min, y_max, y_min):
        super().__init__(name, x_max, x_min, y_max, y_min)
        self.icon = cv2.imread("fuel.png") / 255.0
        self.icon_w = 32
        self.icon_h = 32
        self.icon = cv2.resize(self.icon, (self.icon_h, self.icon_w))


if __name__ == "__main__":
    env = ChopperScape()
    obs = env.reset()
    while True:
        action = env.action_space.sample()
        _, reward, done, info = env.step(action)
        env.render()
        if done:
            break
    # plt.show()
    env.close()
