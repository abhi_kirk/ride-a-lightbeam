import os
import random

import cv2
import numpy as np
from gym import Env, spaces

font = cv2.FONT_HERSHEY_COMPLEX_SMALL

# CANVAS base size from original design
CANVAS_X_BASE = 600
CANVAS_Y_BASE = 800


class CityCabEnv(Env):
    def __init__(self, space_x=600, space_y=800):
        super().__init__()
        self.elements = []
        self.taxi = None
        self.pickup = None
        self.picked_up = False
        self.dropoff = None
        self.ep_return = None
        self.observation_shape = (space_x, space_y, 3)
        self.fuel = 50000
        self.canvas = np.ones(self.observation_shape, dtype=np.float32) * 1
        self.y_min = int(self.observation_shape[0] * 0.1)
        self.x_min = 0
        self.y_max = int(self.observation_shape[0] * 0.9)
        self.x_max = self.observation_shape[1]

        # Scale for new coordinates with respect to base coordinates
        self.canvas_scale = np.array([self.observation_shape[0] / CANVAS_X_BASE,
                                      self.observation_shape[1] / CANVAS_Y_BASE], dtype=np.float32)

        # Fix obstacle, pickup, and drop off location to reduce search space
        self.obstacles_loc = (np.array([[179, 404], [144, 143], [273, 341],
                                        [402, 208], [333, 137], [429, 76], [475, 374], [519, 524],
                                        [187, 444], [325, 385]]) * self.canvas_scale).astype(np.int16)
        self.pickup_loc = (np.array([[129, 328], [105, 133], [141, 430],
                                     [86, 270], [158, 232], [73, 583]]) * self.canvas_scale).astype(np.int16)
        self.dropoff_loc = (np.array([[532, 416], [446, 506], [505, 146], [383, 488],
                                      [453, 408], [462, 239], [580, 550]]) * self.canvas_scale).astype(np.int16)
        self.obstacles_n = self.obstacles_loc.shape[0]
        self.pickup_idx = random.randrange(0, self.pickup_loc.shape[0])
        self.dropoff_idx = random.randrange(0, self.dropoff_loc.shape[0])

        # Number of states determined by taxi x, taxi y, # of pickup locations
        # (+1 for in taxi), # of drop off locations
        num_states = self.observation_shape[0] * self.observation_shape[1] \
                     * (self.pickup_loc.shape[0] + 1) * self.dropoff_loc.shape[0]
        self.observation_space = spaces.Discrete(num_states)
        self.action_space = spaces.Discrete(6, 0)

    def draw_elements_on_canvas(self, state_text="Driving..."):
        self.canvas = np.ones(self.observation_shape, dtype=np.float32) * 1
        for elem in self.elements:
            elem_shape = elem.icon.shape
            x, y = elem.x, elem.y
            self.canvas[y: y + elem_shape[1], x: x + elem_shape[0]] = elem.icon
        text = f'Fuel Left: {self.fuel} | Reward: {self.ep_return} | Status: {state_text}'
        text_coordinate = np.array([10, 20], dtype=np.int16) * self.canvas_scale
        self.canvas = cv2.putText(img=self.canvas, text=text,
                                  org=(int(text_coordinate[0]), int(text_coordinate[1])),
                                  fontFace=font, fontScale=0.8 * self.canvas_scale.min(initial=0),
                                  color=(0, 0, 0),
                                  thickness=1, lineType=cv2.LINE_AA)

    def reset(self, **kwargs):
        self.ep_return = 0
        if "fuel" in kwargs.keys():
            self.fuel = kwargs["fuel"]

        # add taxi
        taxi_x = random.randrange(int(self.observation_shape[0] * 0.05),
                                  int(self.observation_shape[0] * 0.10))
        taxi_y = random.randrange(int(self.observation_shape[1] * 0.15),
                                  int(self.observation_shape[1] * 0.20))
        self.taxi = Taxi("taxi", self.x_max, self.x_min, self.y_max, self.y_min,
                         np.min(self.canvas_scale))
        self.taxi.set_position(taxi_x, taxi_y)
        self.elements = [self.taxi]

        # add obstacles
        for obstacle_idx in range(self.obstacles_n):
            spawned_obstacle = Obstacle("obstacle_{}".format(obstacle_idx),
                                        self.x_max, self.x_min, self.y_max, self.y_min, np.min(self.canvas_scale))
            # self.obstacle_count += 1
            # obstacle_x = random.randrange(self.x_min, self.x_max)
            # obstacle_y = random.randrange(self.y_min, self.y_max)
            obstacle_x = self.obstacles_loc[obstacle_idx, 0]
            obstacle_y = self.obstacles_loc[obstacle_idx, 1]
            spawned_obstacle.set_position(obstacle_x, obstacle_y)
            self.elements.append(spawned_obstacle)

        # add pickup point
        self.pickup = Pickup("pickup", self.x_max, self.x_min, self.y_max,
                             self.y_min, np.min(self.canvas_scale))
        # pickup_x = random.randrange(int(self.observation_shape[0] * 0.80),
        #                             int(self.observation_shape[0] * 0.95))
        # pickup_y = random.randrange(int(self.observation_shape[1] * 0.80),
        #                             int(self.observation_shape[1] * 0.95))
        self.pickup_idx = random.randrange(0, self.pickup_loc.shape[0])
        pickup_x = self.pickup_loc[self.pickup_idx, 0]
        pickup_y = self.pickup_loc[self.pickup_idx, 1]
        self.pickup.set_position(pickup_x, pickup_y)
        self.elements.append(self.pickup)

        # add dropoff point
        self.dropoff = Dropoff("dropoff", self.x_max, self.x_min, self.y_max,
                               self.y_min, np.min(self.canvas_scale))
        # dropoff_x = random.randrange(self.x_min, self.x_max)
        # dropoff_y = random.randrange(self.y_min, self.y_max)
        self.dropoff_idx = random.randrange(0, self.dropoff_loc.shape[0])
        dropoff_x = self.dropoff_loc[self.dropoff_idx, 0]
        dropoff_y = self.dropoff_loc[self.dropoff_idx, 1]
        self.dropoff.set_position(dropoff_x, dropoff_y)
        self.elements.append(self.dropoff)

        self.canvas = np.ones(self.observation_shape) * 1
        self.draw_elements_on_canvas()
        # return self.canvas
        return self.encode()  # return state

    def close(self):
        cv2.destroyAllWindows()

    def render_mode(self, mode="human"):
        assert mode in ["human", "rgb_array"], "Invalid mode, must be either \"human\" or \"rgb_array\""
        # if self.canvas_scale.max() < 1:
        #         scale_x = int(self.canvas.shape[0] / self.canvas_scale[0])
        #         scale_y = int(self.canvas.shape[1] / self.canvas_scale[1])
        #         canvas_resize = cv2.resize(self.canvas, (scale_x, scale_y))
        # else:
        #     canvas_resize = self.canvas
        if mode == "human":
            cv2.imshow("CityCab", self.canvas)
            # cv2.imshow("CityCab", canvas_resize)
            cv2.waitKey(25)
        elif mode == "rgb_array":
            return self.canvas
            # return canvas_resize

    @staticmethod
    def has_collided(elem1, elem2):
        x_col = False
        y_col = False
        elem1_x, elem1_y = elem1.get_position()
        elem2_x, elem2_y = elem2.get_position()
        if 2 * abs(elem1_x - elem2_x) <= (elem1.icon_w + elem2.icon_w):
            x_col = True
        if 2 * abs(elem1_y - elem2_y) <= (elem1.icon_h + elem2.icon_h):
            y_col = True
        if x_col and y_col:
            return True
        return False

    @staticmethod
    def get_action_meanings():
        return {0: "Right", 1: "Left", 2: "Down", 3: "Up",
                4: "Pickup", 5: "Dropoff"}

    def step(self, action_):
        done_ = False
        step_size = 1
        state_text = "Driving..."
        assert self.action_space.contains(action_), "Invalid action"
        self.fuel -= 1
        reward_ = -1
        taxi_move_x, taxi_move_y = 0, 0
        if action_ == 0:
            taxi_move_x, taxi_move_y = 0, step_size
        elif action_ == 1:
            taxi_move_x, taxi_move_y = 0, -step_size
        elif action_ == 2:
            taxi_move_x, taxi_move_y = step_size, 0
        elif action_ == 3:
            taxi_move_x, taxi_move_y = -step_size, 0
        elif action_ == 4:
            if self.pickup in self.elements and self.has_collided(self.taxi, self.pickup):
                self.elements.remove(self.pickup)
                self.pickup_idx = self.pickup_loc[0] + 1
                reward_ = 20
                self.picked_up = True
                state_text = "Picked Up Passenger"
            else:
                reward_ = -10
        elif action_ == 5:
            if self.has_collided(self.taxi, self.dropoff) and self.picked_up:
                reward_ = 20
                done_ = True
                state_text = "Dropped Off Passenger!"
            else:
                reward_ = -10

        if action_ in range(4):
            self.taxi.move(taxi_move_x, taxi_move_y)
            is_new_move_valid = True
            for elem in self.elements:
                if isinstance(elem, Obstacle) and self.has_collided(self.taxi, elem):
                    is_new_move_valid = False
                    break
            if not is_new_move_valid:
                self.taxi.move(-taxi_move_x, -taxi_move_y)  # revert move if hitting obstacles
                state_text = "!Blocked!"

        self.ep_return = reward_
        if self.fuel <= 0:
            done_ = True
            state_text = "Ran Out of Fuel"
        self.draw_elements_on_canvas(state_text=state_text)
        # return self.canvas, reward_, done_, []
        return self.encode(), reward_, done_, []

    def encode(self):
        # encode taxi x, taxi y, pickup index, drop off index into unique state
        # (n_x), n_y, n_pick_up, n_drop_off
        # Note when do encoding, not multiple n_x
        state = self.taxi.x
        state *= self.y_max - self.y_min
        state += self.taxi.y
        state *= self.pickup_loc.shape[0] + 1  # +1 for passenger in taxi
        state += self.pickup_idx
        state *= self.dropoff_loc.shape[0]
        state += self.dropoff_idx
        return state

    def decode(self, state):
        out = []
        n_dropoff_loc = self.dropoff_loc.shape[0]
        n_pickup_loc = self.pickup_loc.shape[0] + 1
        out.append(state % n_dropoff_loc)
        state = state // n_dropoff_loc
        out.append(state % n_pickup_loc)
        state = state // n_pickup_loc
        out.append(state % (self.y_max - self.y_min))
        state = state // (self.y_max - self.y_min)
        out.append(state)
        assert self.x_min <= state < self.x_max
        return reversed(out)


class Point:
    def __init__(self, name, x_max, x_min, y_max, y_min):
        self.icon_h = None
        self.icon_w = None
        self.x = 0
        self.y = 0
        self.x_min = x_min
        self.x_max = x_max
        self.y_min = y_min
        self.y_max = y_max
        self.name = name
        self.icon_path = os.path.dirname(os.path.realpath(__file__))

    @staticmethod
    def clamp(n, minn, maxn):
        return max(min(maxn, n), minn)

    def set_position(self, x, y):
        self.x = self.clamp(x, self.x_min, self.x_max - self.icon_w)
        self.y = self.clamp(y, self.y_min, self.y_max - self.icon_h)

    def get_position(self):
        return self.x, self.y

    def move(self, del_x, del_y):
        self.x += del_x
        self.y += del_y
        self.x = self.clamp(self.x, self.x_min, self.x_max - self.icon_w)
        self.y = self.clamp(self.y, self.y_min, self.y_max - self.icon_h)


class Taxi(Point):
    def __init__(self, name, x_max, x_min, y_max, y_min, scale):
        super().__init__(name, x_max, x_min, y_max, y_min)
        self.icon = cv2.imread(os.path.join(self.icon_path, "cab.jpg")).astype(np.float32) / 255.0
        self.icon_w = int(64 * scale)
        self.icon_h = int(64 * scale)
        self.icon = cv2.resize(self.icon, (self.icon_h, self.icon_w))


class Pickup(Point):
    def __init__(self, name, x_max, x_min, y_max, y_min, scale):
        super().__init__(name, x_max, x_min, y_max, y_min)
        self.icon = cv2.imread(os.path.join(self.icon_path, "pickup.png")).astype(np.float32) / 255.0
        self.icon_w = int(32 * scale)
        self.icon_h = int(32 * scale)
        self.icon = cv2.resize(self.icon, (self.icon_h, self.icon_w))


class Dropoff(Point):
    def __init__(self, name, x_max, x_min, y_max, y_min, scale):
        super().__init__(name, x_max, x_min, y_max, y_min)
        self.icon = cv2.imread(os.path.join(self.icon_path, "dropoff.png")).astype(np.float32) / 255.0
        self.icon_w = int(32 * scale)
        self.icon_h = int(32 * scale)
        self.icon = cv2.resize(self.icon, (self.icon_h, self.icon_w))


class Obstacle(Point):
    def __init__(self, name, x_max, x_min, y_max, y_min, scale):
        super().__init__(name, x_max, x_min, y_max, y_min)
        self.icon = cv2.imread(os.path.join(self.icon_path, "obstacle.png")).astype(np.float32) / 255.0
        self.icon_w = int(32 * scale)
        self.icon_h = int(32 * scale)
        self.icon = cv2.resize(self.icon, (self.icon_h, self.icon_w))


if __name__ == "__main__":
    env = CityCabEnv()
    obs = env.reset(fuel=10000)
    while True:
        action = env.action_space.sample()
        _, reward, done, info = env.step(action)
        env.render_mode()
        if done:
            input("Hit Enter to Exit.")
            break
    env.close()
