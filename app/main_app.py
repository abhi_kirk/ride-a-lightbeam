"""Build streamlit app for Taxi Example Using Reinforcement Learning
"""
import os
import sys

import streamlit as st

# Add custom modules under src folder
sys.path.append(os.path.join(os.getcwd(), "src"))
from analysis import Analysis
from model import Model

st.header("Self-driving Taxi Using Reinforcement Learning")
st.write(
    "This application uses customized taxi environment"
    " to train a Reinforcement Learning (RL) algorithm (Q-learning)"
)

st.subheader("Train and Evaluate RL Algorithm")
with st.form("train_eval_parameters"):
    col_env, col_train, col_eval = st.columns(3)
    with col_env:
        st.subheader("Taxi Environment")
        env_space_x = st.number_input('Dimension x:', min_value=200,
                                      max_value=2000, value=300, step=100, format='%d',
                                      key='env_space_x',
                                      help='Taxi environment coordinate x')
        env_space_y = st.number_input('Dimension y:', min_value=200,
                                      max_value=2000, value=400, step=100, format='%d',
                                      key='env_space_y',
                                      help='Taxi environment coordinate y')
        env_taxi_fuel = st.number_input('Taxi fuel:', min_value=100,
                                        max_value=500000, value=1000, step=100, format='%d',
                                        key='env_taxi_fuel',
                                        help='Taxi will stop if running out of fuel')

    with col_train:
        st.subheader("Training")
        n_episodes_train = st.number_input('Number of episodes:', min_value=0,
                                           max_value=300, value=1, step=10,
                                           key='n_episodes_train',
                                           help="Each episode represents one complete session \
            and is independent from other episodes.")
        max_steps_train = st.number_input(
            "Maximum number of steps per episode:", min_value=0,
            max_value=3000, value=1000, step=100, key='max_steps_train')
        exploration_probability_train = st.slider(
            "Exploration probability:", 0.0, 1.0, 0.2, 0.01,
            key="exploration_probability_train",
            help="probability on when to select exploration over exploitation.",
        )
        learning_rate_train = st.slider(
            "Learning rate:", 0.0, 1.0, 0.7, 0.01,
            help="How much to accept new information vs. old information \
                (0: no update from new information).",
        )
        discount_rate_train = st.slider(
            "Discount rate:", 0.0, 1.0, 0.62, 0.01,
            help="Discount the \
                future rewards.",
        )
        load_prev_training = st.checkbox('Use previous training model', value=False)
        training_model_path = st.text_input('Training file name:',
                                            value='q_table.npy',
                                            help='File name to store Q table. When loading previous training is \
                selected, the Q table will be update from this file.')

    with col_eval:
        st.subheader("Evaluate")
        n_episodes_test = st.number_input('Number of episodes:', min_value=0,
                                          max_value=300, value=1, step=10,
                                          key='n_episodes_test',
                                          help="Each episode represents one complete session \
            and is independent from other episodes.")
        max_steps_test = st.number_input(
            "Maximum number of steps per episode:", min_value=0,
            key='max_steps_test',
            max_value=3000, value=100, step=100)
        exploration_probability_test = st.slider(
            "Exploration probability:",
            0.0,
            1.0,
            0.1,
            0.01,
            help="probability on when to select exploration over exploitation.",
        )

    # Every form must have a submit button.
    submitted = st.form_submit_button("Start")
    if submitted:
        model = Model(env_space_x, env_space_y)
        analysis = Analysis()
        with st.spinner("Training..."):
            rewards_train = model.train(
                n_episodes=n_episodes_train,
                max_steps=max_steps_train,
                learning_rate=learning_rate_train,
                discount_rate=discount_rate_train,
                exploration_probability=exploration_probability_train,
                use_prev_training_model=load_prev_training,
                training_model_file=training_model_path,
                taxi_fuel=env_taxi_fuel,
            )
        st.success("Training is done!")

        with st.spinner("Evaluating..."):
            rewards_test, frames = model.evaluate(
                n_episodes=n_episodes_test,
                max_steps=max_steps_test,
                exploration_probability=exploration_probability_test,
                taxi_fuel=env_taxi_fuel,
            )
        st.success("Evaluation is done!")

        with st.spinner("Generating animation..."):
            episode_k = 1
            analysis.generate_animation_video(frames, episode_k)
            is_animation_generated = True
        st.success("Animation generation is done!")
    else:
        is_animation_generated = False
        st.warning("Please click start button.")

st.subheader("Self-Driving Taxi Animation")
st.write(
    "A taxi will pick up a passenger and drop the passenger at a specific destination\
    by finding a optimal route and avoiding obstacles."
)
st.write(
    "Each time, the pickup and drop off locations will be randomly \
    selected from 7 locations. The start position of taxi is random."
)
if is_animation_generated:
    video_file = open('city_cab.mp4', 'rb')
    video_bytes = video_file.read()
    st.video(video_bytes)
else:
    st.warning("No animation yet. Please start training first")
