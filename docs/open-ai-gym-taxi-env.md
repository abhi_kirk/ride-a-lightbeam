# [OpenAI Gym Taxi Environment](https://github.com/openai/gym/blob/master/gym/envs/toy_text/taxi.py)
![Taxi without passenger](/docs/images/taxi_without_passenger.png)
![Taxi with passenger](/docs/images/taxi_with_passenger.png)

## Description
OpenAi Gym provides a text-based taxi environment, `Taxi-v3`. When the episode starts, the taxi starts off at a random
square of the grid, drives to the passenger's location (one of the four specified locations indicated by R, G, Y, and B), picks up the passenger,
drives to the passenger's destination (another one of the four specified locations), and then drop off the passenger. Once the passenger is dropped off, the episode ends.

## Rewards:
`env.P` is a reward table (states x actions matrix) with the number of states as rows and number of actions as columns
Each element `env.P[num]` is a dictionary with the structure `{action: [(probability, nextstate, reward, done)]}`
- The action has the value from 0 to 5 (south, north, east, west, pickup, dropoff)
- The probability is always 1.0 in this env
- The nextstate is the state the taxi would be in if taking the action specified in that row
- The reward has the following values
  - -1: each action
  - -10: executing actions "pickup" and "dropoff" wrong
  - +20: delivering the passenger successfully
- done indicates a successful dropoff and also the end of an episode

## State Space
The state space is the set of all possible situations the taxi could inhabit. Assume the taxi is the only vehicle
in his parking lot. We can break up the parking lot into a 5x5 grid, which gives 25 possible taxi locations. There are 5
(4 + 1) passenger location, consisting of 4 locations and 1 additional passenger state of being inside the taxi. There
are 4 destinations. So the state space can be represented by:
(taxi_row, taxi_col, passenger_location, destination)
with total 500(5x5x5x4) possible states.

**Note** that there are $400$ states that can actually be reached during an episode. The missing $100$ states correspond to situations in which the passenger is at the same location as their destination since this typically signals the end of an episode.

Some useful commands from environment:
- `env.encode(3, 1, 2, 0)` converts state (taxi row, taxi column, passenger, destination) to index
- `env.s` shows the current state in index

## Actions:
6 possible actions:
- 0: move south
- 1: move north
- 2: move east
- 3: move west
- 4: pickup passenger
- 5: dropoff passenger
    Each successful dropoff is the end of an episode

## Rendering:
- Filled square: taxi (yellow - no passenger, green - with a passenger)
- The pipe ('"|") represents a wall which the taxi cannot cross
- R, G, B, Y are the possible pickup and destination locations. The blue letter represents the pick-up location and
the purple letter is the current destination drop-off destination

## Tips
[Change the maximum number steps when environment stops](https://stackoverflow.com/questions/42787924/why-is-episode-done-after-200-time-steps-gym-environment-mountaincar/42802225#42802225)

[How to export the run to a video file?](https://github.com/openai/gym/wiki/FAQ)
Note it may only generates .json file instead of .mp4 file after installing ffmpeg as explained in the [stackoverflow answer](https://stackoverflow.com/questions/54100582/saving-a-video-gif-file-for-the-open-ai-gymtaxi-environment). In the same answer page, it mentioned using `asciinema` to play the .json file generated by the gym. It works well.
