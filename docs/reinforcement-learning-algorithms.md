# Reinforcement Learning Algorithm
Learn a mapping of states (500) to the optimal action (6).

## Q-learning algorithm
Refer to Ch6 or Sutton & Barto (2018). The value store in the Q-table are called a Q-values, and they map to a
(state, action) combination. Better Q-values imply better chances of getting greater rewards. Q-values are initialized
to an arbitrary value and are updated using the equation:
$$Q(state, action) = (1 - \alpha) Q(state, action) + \alpha (reward + \gamma \max_a Q(next state, action) -
Q(state, action))$$

where
- $\alpha$ (alpha) is the learning rate ($0 < alpha \leq 1$): how easily the agent should accept the new information
- $\gamma$ (gamma) is the discount factor ($0 \leq \gamma \leq 1$): how much importance to give to future rewards. A
high value (close to 1) capture the long-term effective award while a low value (close to 0) makes the agent consider
immediate reward, therefore making it greedy.

## Exploration vs. Exploitation

## Implementation
- Initialize the Q-table with 0's for all Q-values
- Let agent play Taxi over a large number of games and update the Q-table using the Q-learning algorithm and an exploration strategy
