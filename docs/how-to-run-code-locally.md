# How to Run Code Locally

1. In IDE, set `ride-a-lightbeam` folder as the working folder
![pychar project working folder](images/pycharm-project-working-folder.png)

2. Open `src/main.py` and run and test the code locally
* Modify training and evaluation parameters in `config/main.yml`  
    - Under `env`, `space_x` and `space_y` determines canvas size of custom environment and set `taxi_fuel` larger than `space_x * space_y` (at least 2x for training)
    - Under `training`, set `max_steps` larger than `space_x * space_y` (at least 2x for training), better to match `taxi_fuel`
* Environment code is in `custom_envs` folder
