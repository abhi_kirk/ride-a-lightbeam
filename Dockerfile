# Load parent image from official python image
# Use `-slim` version for a smaller container size
FROM python:3.9-slim

# Update the system
RUN apt-get update
# Install python cv2 dependency
RUN apt-get install ffmpeg libsm6 libxext6 -y

# Set a working directory
WORKDIR /ride-a-lightbeam

# Copy requirements first
COPY requirements.txt ./

# Install dependencies
RUN python3 -m pip install --upgrade pip setuptools wheel
RUN python3 -m pip install -r requirements.txt && rm -rf /root/.cache

# Copy source files
COPY src/*.py ./src/
COPY app/*.py ./app/
COPY custom_envs/cab_env/ ./custom_envs/cab_env/

# Create shared volume
VOLUME qtable

# Port number the container exposes
EXPOSE 8501

# Run the command
CMD ["streamlit", "run", "./app/main_app.py"]
