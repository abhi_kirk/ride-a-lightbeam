# Ride a Lightbeam

## Summary
Dynamic route optimization for self-driving cabs using Reinforcement Learning algorithms.

## Objectives
- Get up to speed with latest Python data science tools
- Utilize OO framework
- Agile approach with Git, CI/CD, Jira
- Studying and implementing various RL algorithms
- Platform independent dashboard deployment
- Open-source for community support

## Dashboard
A web application with taxi animation is built with [Streamlit](https://streamlit.io) framework and can be ran in different ways:
- To run it locally: under python environment, run `streamlit run main_streamlit_app.py` in `src` folder
- To run it via container:
    - Build docker image via [Dockerfile](Dockerfile) and
    - `docker run -dp 8501:8501 image-name`
    - Open a web browser and open the link `localhost:8501`
- To run it via an [AWS application](http://ridealightbeam-env.eba-gmw7x4cp.us-east-1.elasticbeanstalk.com/), using the [AWS docker configruation](Dockerrun.aws.json)

![streamlit dashboard train](/docs/images/streamlit-dashboard-train-section.png)
![streamlit dashboard animation](/docs/images/streamlit-dashboard-animation-section.png)

OpenAI environment saves ASCII frame which can be printed to terminal to show the movement of taxi. In this project, a customize taxi animation rendered using matplotlib is added.

## Specifications
- python v3.8.8
- requirements.txt for venv

## Additional Notes
- [Introduction to Open AI gym taxi environment](/docs/open-ai-gym-taxi-env.md)
- [Reinforcement learning algorithms](/docs/reinforcement-learning-algorithms.md)
- [How to run code locally](/docs/how-to-run-code-locally.md)
